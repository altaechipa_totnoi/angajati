package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.validator.EmployeeValidator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeImplTest {
    private EmployeeImpl employeeRepository;
    private EmployeeValidator employeeValidator = new EmployeeValidator();

    @Before
    public void setUp() {
        employeeRepository = new EmployeeImpl();
    }

    @After
    public void tearDown() {
        employeeRepository = null;
    }

    /**
     * Test TC1.
     *
     *Expected: employee added.
     */
    @Test
    public void insertShouldCorrectlyAddEmployee(){

        Employee employee = new Employee();
        employee.setFirstName("Ana");
        employee.setLastName("Pop");
        employee.setCnp("2890908270054");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2000.0);

        assertTrue(employeeRepository.addEmployee(employee));
    }

    /**
     * Test TC2.
     *
     * Test for last name.
     * Expected: employee not added.
     */
    @Test
    public void insertShouldNotAddEmployeeLastNameError() {

        Employee employee = new Employee();
        employee.setFirstName("Ana");
        employee.setLastName("Po");
        employee.setCnp("2890908270054");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2000.0);

        assertFalse(employeeRepository.addEmployee(employee));

    }

    /**
     * Test TC3.
     *
     *Test for first name.
     * Expected: employee not added  -  invalid first name.
     */
    @Test
    public void insertShouldNotAddEmployeeFirstName() {

        Employee employee = new Employee();
        employee.setFirstName("An");
        employee.setLastName("Pop");
        employee.setCnp("2890908270054");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2000.0);

        assertFalse(employeeRepository.addEmployee(employee));
    }

    /**
     * Test TC4.
     *
     * Test for last name BVA.
     * Expected: employee added.
     */
    @Test
    public void insertShouldCorrectlyAddEmployee1BVA() {

        Employee employee = new Employee();
        employee.setFirstName("Ana");
        employee.setLastName("Popa");
        employee.setCnp("2890908270054");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2000.0);

        assertTrue(employeeRepository.addEmployee(employee));
    }

    /**
     * Test TC5
     *
     * Test for last name BVA.
     * Expected: employee added.
     */
    @Test
    public void insertShouldCorrectlyAddEmployee2BVA() {

        Employee employee = new Employee();
        employee.setFirstName("Anca");

        employee.setLastName("Popa");
        employee.setCnp("2890908270054");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2000.0);

        assertTrue(employeeRepository.addEmployee(employee));
    }


    /**
     * Test TC6
     *
     * Test for last name BVA.
     * Expected: employee added.
     */
    @Test
    public void insertShouldCorrectlyAddEmployeeLastNameBVA() {

        Employee employee = new Employee();
        employee.setFirstName("Ana");

        String lastName = "";
        for (int i= 0; i<255; i++){
            lastName = lastName + "a";
        }

        System.out.print(lastName.length());

        employee.setLastName(lastName);
        employee.setCnp("2890908270054");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2000.0);

        assertTrue(employeeRepository.addEmployee(employee));
    }


    /**
     * Test TC7
     *
     * Test for last name BVA.
     * Expected: employee added.
     */
    @Test
    public void insertShouldCorrectlyAddEmployee4BVA() {

        Employee employee = new Employee();
        employee.setFirstName("Ana");

        String lastName = "";
        for (int i= 0; i<256; i++){
            lastName = lastName + "a";
        }

        System.out.print(lastName.length());

        employee.setLastName(lastName);
        employee.setCnp("2890908270054");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2000.0);

        assertTrue(employeeRepository.addEmployee(employee));
    }

    /**
     * Test TC8
     *
     * Test for last name BVA.
     * Expected: employee added.
     */

    /**
     * To do
     */
    @Test
    public void insertShouldNotAddEmployee6BVA() {

        Employee employee = new Employee();
        employee.setFirstName("Ana");

        String lastName = "";
        for (int i= 0; i<257; i++){
            lastName = lastName + "a";
        }

        System.out.print(lastName.length());

        employee.setLastName(lastName);
        employee.setCnp("2890908270054");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2000.0);

        assertFalse(employeeRepository.addEmployee(employee));
    }


    /**
     * Test TC9.
     *
     * Test for CNP.
     *Expected: employee not added
     */
    @Test
    public void insertShouldNotAddEmployeeCNPError1() {

        Employee employee = new Employee();
        employee.setFirstName("Ana");
        employee.setLastName("Pop");
        employee.setCnp("28909082700541");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2000.0);

        assertFalse(employeeRepository.addEmployee(employee));
    }

    /**
     * Test TC10.
     *
     * Test for CNP.
     *Expected: employee not added
     */
    @Test
    public void insertShouldNotAddEmployeeCNPError2() {

        Employee employee = new Employee();
        employee.setFirstName("Ana");
        employee.setLastName("Pop");
        employee.setCnp("a890908270054");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2000.0);

        assertFalse(employeeRepository.addEmployee(employee));
    }

    /**
     * Test TC11.
     *
     * Test for CNP.
     *Expected: employee not added
     */
    @Test
    public void insertShouldNotAddEmployeeCNPError3() {

        Employee employee = new Employee();
        employee.setFirstName("Ana");
        employee.setLastName("Pop");
        employee.setCnp("289090827005");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2000.0);

        assertFalse(employeeRepository.addEmployee(employee));
    }

    /**
     * Test TC12.
     *
     *Test for Salary.
     *Expected: employee not added.
     */
    @Test
    public void insertShouldNotAddEmployeeSalary() {

        Employee employee = new Employee();
        employee.setFirstName("Ana");
        employee.setLastName("Pop");
        employee.setCnp("2890908270054");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(0.0);

        assertFalse(employeeRepository.addEmployee(employee));
    }

    /**
     * Test TC13.
     *
     *Test for salaty.
     * Expected: employee added.
     */
    @Test
    public void insertShouldCorrectlyAddEmployeeSalaryBVA1() {

        Employee employee = new Employee();
        employee.setFirstName("Ana");
        employee.setLastName("Pop");
        employee.setCnp("2890908270054");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(0.1);

        assertTrue(employeeRepository.addEmployee(employee));
    }

    /**
     * Test TC14.
     *
     *Test for salaty.
     * Expected: employee added.
     */
    @Test
    public void insertShouldCorrectlyAddEmployeeSalaryBVA2() {

        Employee employee = new Employee();
        employee.setFirstName("Ana");
        employee.setLastName("Pop");
        employee.setCnp("2890908270054");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(1.0);

        assertTrue(employeeRepository.addEmployee(employee));
    }
}