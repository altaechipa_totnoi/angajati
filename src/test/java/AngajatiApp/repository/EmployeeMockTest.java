package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class EmployeeMockTest {
    private EmployeeMock employeeRepository;

    @Before
    public void setUp() throws Exception {
        employeeRepository = new EmployeeMock();
    }

    @After
    public void tearDown() throws Exception {
        employeeRepository = null;
    }

    @Test
    public void testTC1() {
        Employee Ionel = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        Employee Mihai = new Employee("Ion", "Dumitrescu", "1234567890876", DidacticFunction.LECTURER, 2500d);
        employeeRepository.addEmployee(Ionel);
        employeeRepository.addEmployee(Mihai);

        employeeRepository.modifyEmployeeFunction(Ionel, DidacticFunction.TEACHER);

        DidacticFunction newFunction = employeeRepository.findEmployeeById(Ionel.getId()).getFunction();

        assertEquals(newFunction, DidacticFunction.TEACHER);
    }
    @Test
    public void TestC2 (){
        Employee Ionel = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        Employee Mihai = new Employee("Ion", "Dumitrescu", "1234567890876", DidacticFunction.LECTURER, 2500d);
        Employee Ana = null;

        employeeRepository.addEmployee(Ionel);
        employeeRepository.addEmployee(Mihai);

        String oldEmployeeList = employeeRepository.getEmployeeList().toString();

        employeeRepository.modifyEmployeeFunction(Ana, DidacticFunction.TEACHER);

        assertTrue(oldEmployeeList.equals(employeeRepository.getEmployeeList().toString()));
    }

}